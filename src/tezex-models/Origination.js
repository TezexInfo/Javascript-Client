/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'tezex-models/OriginationOperation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./OriginationOperation'));
  } else {
    // Browser globals (root is window)
    if (!root.TezexApi) {
      root.TezexApi = {};
    }
    root.TezexApi.Origination = factory(root.TezexApi.ApiClient, root.TezexApi.OriginationOperation);
  }
}(this, function(ApiClient, OriginationOperation) {
  'use strict';




  /**
   * The Origination model module.
   * @module tezex-models/Origination
   * @version 0.0.2
   */

  /**
   * Constructs a new <code>Origination</code>.
   * @alias module:tezex-models/Origination
   * @class
   */
  var exports = function() {
    var _this = this;











  };

  /**
   * Constructs a <code>Origination</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:tezex-models/Origination} obj Optional instance to populate.
   * @return {module:tezex-models/Origination} The populated <code>Origination</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('hash')) {
        obj['hash'] = ApiClient.convertToType(data['hash'], 'String');
      }
      if (data.hasOwnProperty('branch')) {
        obj['branch'] = ApiClient.convertToType(data['branch'], 'String');
      }
      if (data.hasOwnProperty('source')) {
        obj['source'] = ApiClient.convertToType(data['source'], 'String');
      }
      if (data.hasOwnProperty('public_key')) {
        obj['public_key'] = ApiClient.convertToType(data['public_key'], 'String');
      }
      if (data.hasOwnProperty('fee')) {
        obj['fee'] = ApiClient.convertToType(data['fee'], 'Integer');
      }
      if (data.hasOwnProperty('counter')) {
        obj['counter'] = ApiClient.convertToType(data['counter'], 'Integer');
      }
      if (data.hasOwnProperty('operations')) {
        obj['operations'] = ApiClient.convertToType(data['operations'], [OriginationOperation]);
      }
      if (data.hasOwnProperty('level')) {
        obj['level'] = ApiClient.convertToType(data['level'], 'Integer');
      }
      if (data.hasOwnProperty('block_hash')) {
        obj['block_hash'] = ApiClient.convertToType(data['block_hash'], 'String');
      }
      if (data.hasOwnProperty('time')) {
        obj['time'] = ApiClient.convertToType(data['time'], 'Date');
      }
    }
    return obj;
  }

  /**
   * @member {String} hash
   */
  exports.prototype['hash'] = undefined;
  /**
   * @member {String} branch
   */
  exports.prototype['branch'] = undefined;
  /**
   * @member {String} source
   */
  exports.prototype['source'] = undefined;
  /**
   * @member {String} public_key
   */
  exports.prototype['public_key'] = undefined;
  /**
   * @member {Integer} fee
   */
  exports.prototype['fee'] = undefined;
  /**
   * @member {Integer} counter
   */
  exports.prototype['counter'] = undefined;
  /**
   * @member {Array.<module:tezex-models/OriginationOperation>} operations
   */
  exports.prototype['operations'] = undefined;
  /**
   * @member {Integer} level
   */
  exports.prototype['level'] = undefined;
  /**
   * @member {String} block_hash
   */
  exports.prototype['block_hash'] = undefined;
  /**
   * @member {Date} time
   */
  exports.prototype['time'] = undefined;



  return exports;
}));


