/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'tezex-models/TezosScript'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./TezosScript'));
  } else {
    // Browser globals (root is window)
    if (!root.TezexApi) {
      root.TezexApi = {};
    }
    root.TezexApi.TransactionOperation = factory(root.TezexApi.ApiClient, root.TezexApi.TezosScript);
  }
}(this, function(ApiClient, TezosScript) {
  'use strict';




  /**
   * The TransactionOperation model module.
   * @module tezex-models/TransactionOperation
   * @version 0.0.2
   */

  /**
   * Constructs a new <code>TransactionOperation</code>.
   * @alias module:tezex-models/TransactionOperation
   * @class
   */
  var exports = function() {
    var _this = this;





  };

  /**
   * Constructs a <code>TransactionOperation</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:tezex-models/TransactionOperation} obj Optional instance to populate.
   * @return {module:tezex-models/TransactionOperation} The populated <code>TransactionOperation</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('kind')) {
        obj['kind'] = ApiClient.convertToType(data['kind'], 'String');
      }
      if (data.hasOwnProperty('amount')) {
        obj['amount'] = ApiClient.convertToType(data['amount'], 'Integer');
      }
      if (data.hasOwnProperty('destination')) {
        obj['destination'] = ApiClient.convertToType(data['destination'], 'String');
      }
      if (data.hasOwnProperty('parameters')) {
        obj['parameters'] = TezosScript.constructFromObject(data['parameters']);
      }
    }
    return obj;
  }

  /**
   * @member {String} kind
   */
  exports.prototype['kind'] = undefined;
  /**
   * @member {Integer} amount
   */
  exports.prototype['amount'] = undefined;
  /**
   * @member {String} destination
   */
  exports.prototype['destination'] = undefined;
  /**
   * @member {module:tezex-models/TezosScript} parameters
   */
  exports.prototype['parameters'] = undefined;



  return exports;
}));


