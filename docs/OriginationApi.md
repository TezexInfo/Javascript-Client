# TezexApi.OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOrigination**](OriginationApi.md#getOrigination) | **GET** /origination/{origination_hash} | Get Origination


<a name="getOrigination"></a>
# **getOrigination**
> Origination getOrigination(originationHash)

Get Origination

Get a specific Origination

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.OriginationApi();

var originationHash = "originationHash_example"; // String | The hash of the Origination to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getOrigination(originationHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **originationHash** | **String**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

