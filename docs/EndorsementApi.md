# TezexApi.EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEndorsement**](EndorsementApi.md#getEndorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**getEndorsementForBlock**](EndorsementApi.md#getEndorsementForBlock) | **GET** /endorsement/for/{block_hash} | Get Endorsement


<a name="getEndorsement"></a>
# **getEndorsement**
> Endorsement getEndorsement(endorsementHash)

Get Endorsement

Get a specific Endorsement

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.EndorsementApi();

var endorsementHash = "endorsementHash_example"; // String | The hash of the Endorsement to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getEndorsement(endorsementHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsementHash** | **String**| The hash of the Endorsement to retrieve | 

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getEndorsementForBlock"></a>
# **getEndorsementForBlock**
> [Endorsement] getEndorsementForBlock(blockHash)

Get Endorsement

Get a specific Endorsement

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.EndorsementApi();

var blockHash = "blockHash_example"; // String | blockhash


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getEndorsementForBlock(blockHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockHash** | **String**| blockhash | 

### Return type

[**[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

