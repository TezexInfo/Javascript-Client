# TezexApi.Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  | [optional] 
**operationCount** | **Integer** |  | [optional] 
**sentTransactionCount** | **Integer** |  | [optional] 
**recvTransactionCount** | **Integer** |  | [optional] 
**originationCount** | **Integer** |  | [optional] 
**delegationCount** | **Integer** |  | [optional] 
**delegatedCount** | **Integer** |  | [optional] 
**endorsementCount** | **Integer** |  | [optional] 
**firstSeen** | **Date** |  | [optional] 
**lastSeen** | **Date** |  | [optional] 
**name** | **String** |  | [optional] 
**balance** | **String** |  | [optional] 
**totalSent** | **String** |  | [optional] 
**totalReceived** | **String** |  | [optional] 
**bakedBlocks** | **Integer** |  | [optional] 
**imageUrl** | **String** |  | [optional] 


