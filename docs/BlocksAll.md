# TezexApi.BlocksAll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  | [optional] 
**blocks** | [**[Block]**](Block.md) |  | [optional] 
**totalResults** | **Integer** |  | [optional] 
**currentPage** | **Integer** |  | [optional] 


