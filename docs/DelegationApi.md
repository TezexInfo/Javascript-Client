# TezexApi.DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDelegation**](DelegationApi.md#getDelegation) | **GET** /delegation/{delegation_hash} | Get Delegation


<a name="getDelegation"></a>
# **getDelegation**
> Delegation getDelegation(delegationHash)

Get Delegation

Get a specific Delegation

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.DelegationApi();

var delegationHash = "delegationHash_example"; // String | The hash of the Origination to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDelegation(delegationHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegationHash** | **String**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

