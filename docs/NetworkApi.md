# TezexApi.NetworkApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**network**](NetworkApi.md#network) | **GET** /network | Get Network Information


<a name="network"></a>
# **network**
> NetworkInfo network()

Get Network Information

Get Network Information

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.NetworkApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.network(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

