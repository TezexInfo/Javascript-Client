# TezexApi.BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocksAll**](BlockApi.md#blocksAll) | **GET** /blocks/all | Get All Blocks 
[**blocksByLevel**](BlockApi.md#blocksByLevel) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocksByLevelRange**](BlockApi.md#blocksByLevelRange) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**getBlock**](BlockApi.md#getBlock) | **GET** /block/{blockhash} | Get Block By Blockhash
[**getBlockDelegations**](BlockApi.md#getBlockDelegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**getBlockEndorsements**](BlockApi.md#getBlockEndorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**getBlockOperationsSorted**](BlockApi.md#getBlockOperationsSorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**getBlockOriginations**](BlockApi.md#getBlockOriginations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**getBlockTransaction**](BlockApi.md#getBlockTransaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recentBlocks**](BlockApi.md#recentBlocks) | **GET** /blocks/recent | returns the last 25 blocks


<a name="blocksAll"></a>
# **blocksAll**
> BlocksAll blocksAll(opts)

Get All Blocks 

Get all Blocks

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var opts = { 
  'page': 3.4, // Number | Pagination, 200 tx per page max
  'order': "order_example", // String | ASC or DESC
  'limit': 56 // Integer | Results per Page
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.blocksAll(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="blocksByLevel"></a>
# **blocksByLevel**
> [Block] blocksByLevel(level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var level = 3.4; // Number | The level of the Blocks to retrieve, includes abandoned


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.blocksByLevel(level, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **Number**| The level of the Blocks to retrieve, includes abandoned | 

### Return type

[**[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="blocksByLevelRange"></a>
# **blocksByLevelRange**
> BlockRange blocksByLevelRange(startlevel, stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var startlevel = 3.4; // Number | lowest blocklevel to return

var stoplevel = 3.4; // Number | highest blocklevel to return


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.blocksByLevelRange(startlevel, stoplevel, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Number**| lowest blocklevel to return | 
 **stoplevel** | **Number**| highest blocklevel to return | 

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlock"></a>
# **getBlock**
> Block getBlock(blockhash)

Get Block By Blockhash

Get a block by its hash

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | The hash of the Block to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlock(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve | 

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockDelegations"></a>
# **getBlockDelegations**
> [Delegation] getBlockDelegations(blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | Blockhash


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlockDelegations(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockEndorsements"></a>
# **getBlockEndorsements**
> [Endorsement] getBlockEndorsements(blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | Blockhash


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlockEndorsements(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockOperationsSorted"></a>
# **getBlockOperationsSorted**
> BlockOperationsSorted getBlockOperationsSorted(blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | The hash of the Block to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlockOperationsSorted(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| The hash of the Block to retrieve | 

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockOriginations"></a>
# **getBlockOriginations**
> [Origination] getBlockOriginations(blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | Blockhash


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlockOriginations(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**[Origination]**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getBlockTransaction"></a>
# **getBlockTransaction**
> [Transaction] getBlockTransaction(blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var blockhash = "blockhash_example"; // String | Blockhash


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBlockTransaction(blockhash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **String**| Blockhash | 

### Return type

[**[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="recentBlocks"></a>
# **recentBlocks**
> [Block] recentBlocks()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.recentBlocks(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

