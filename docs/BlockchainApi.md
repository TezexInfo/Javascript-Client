# TezexApi.BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


<a name="blockheight"></a>
# **blockheight**
> Level blockheight()

Get Max Blockheight

Get the maximum Level we have seen

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.BlockchainApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.blockheight(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

