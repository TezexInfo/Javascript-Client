# TezexApi.OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOperation**](OperationApi.md#getOperation) | **GET** /operation/{operation_hash} | Get Operation


<a name="getOperation"></a>
# **getOperation**
> Operation getOperation(operationHash)

Get Operation

Get a specific Operation

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.OperationApi();

var operationHash = "operationHash_example"; // String | The hash of the Operation to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getOperation(operationHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operationHash** | **String**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

