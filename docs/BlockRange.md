# TezexApi.BlockRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  | [optional] 
**blocks** | [**[Block]**](Block.md) |  | [optional] 


