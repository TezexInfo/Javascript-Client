# TezexApi.AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccount**](AccountApi.md#getAccount) | **GET** /account/{account} | Get Account
[**getAccountBalance**](AccountApi.md#getAccountBalance) | **GET** /account/{account}/balance | Get Account Balance
[**getAccountLastSeen**](AccountApi.md#getAccountLastSeen) | **GET** /account/{account}/last_seen | Get last active date
[**getAccountOperationCount**](AccountApi.md#getAccountOperationCount) | **GET** /account/{account}/operations_count | Get operation count of Account
[**getAccountTransactionCount**](AccountApi.md#getAccountTransactionCount) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**getDelegationsForAccount**](AccountApi.md#getDelegationsForAccount) | **GET** /account/{account}/delegations | Get Delegations of this account
[**getDelegationsToAccount**](AccountApi.md#getDelegationsToAccount) | **GET** /account/{account}/delegated | Get Delegations to this account
[**getEndorsementsForAccount**](AccountApi.md#getEndorsementsForAccount) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**getTransactionForAccountIncoming**](AccountApi.md#getTransactionForAccountIncoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**getTransactionForAccountOutgoing**](AccountApi.md#getTransactionForAccountOutgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


<a name="getAccount"></a>
# **getAccount**
> Account getAccount(account)

Get Account

Get Acccount

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccount(account, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountBalance"></a>
# **getAccountBalance**
> &#39;String&#39; getAccountBalance(account)

Get Account Balance

Get Balance

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountBalance(account, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**&#39;String&#39;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountLastSeen"></a>
# **getAccountLastSeen**
> &#39;Date&#39; getAccountLastSeen(account)

Get last active date

Get LastSeen Date

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountLastSeen(account, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**&#39;Date&#39;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountOperationCount"></a>
# **getAccountOperationCount**
> &#39;Integer&#39; getAccountOperationCount(account)

Get operation count of Account

Get Operation Count

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountOperationCount(account, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**&#39;Integer&#39;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getAccountTransactionCount"></a>
# **getAccountTransactionCount**
> &#39;Integer&#39; getAccountTransactionCount(account)

Get transaction count of Account

Get Transaction Count

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAccountTransactionCount(account, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account | 

### Return type

**&#39;Integer&#39;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getDelegationsForAccount"></a>
# **getDelegationsForAccount**
> [Delegation] getDelegationsForAccount(account, opts)

Get Delegations of this account

Get Delegations this Account has made

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account for which to retrieve Delegations

var opts = { 
  'before': 56 // Integer | Only Return Delegations before this blocklevel
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDelegationsForAccount(account, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Delegations | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getDelegationsToAccount"></a>
# **getDelegationsToAccount**
> [Delegation] getDelegationsToAccount(account, opts)

Get Delegations to this account

Get that have been made to this Account

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account to which delegations have been made

var opts = { 
  'before': 56 // Integer | Only Return Delegations before this blocklevel
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDelegationsToAccount(account, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account to which delegations have been made | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getEndorsementsForAccount"></a>
# **getEndorsementsForAccount**
> [Endorsement] getEndorsementsForAccount(account, opts)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account for which to retrieve Endorsements

var opts = { 
  'before': 56 // Integer | Only Return Delegations before this blocklevel
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getEndorsementsForAccount(account, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve Endorsements | 
 **before** | **Integer**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionForAccountIncoming"></a>
# **getTransactionForAccountIncoming**
> Transactions getTransactionForAccountIncoming(account, opts)

Get Transaction

Get incoming Transactions for a specific Account

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account for which to retrieve incoming Transactions

var opts = { 
  'before': 56 // Integer | Only Return transactions before this blocklevel
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransactionForAccountIncoming(account, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve incoming Transactions | 
 **before** | **Integer**| Only Return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionForAccountOutgoing"></a>
# **getTransactionForAccountOutgoing**
> Transactions getTransactionForAccountOutgoing(account, opts)

Get Transaction

Get outgoing Transactions for a specific Account

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.AccountApi();

var account = "account_example"; // String | The account for which to retrieve outgoing Transactions

var opts = { 
  'before': 56 // Integer | Only return transactions before this blocklevel
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransactionForAccountOutgoing(account, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| The account for which to retrieve outgoing Transactions | 
 **before** | **Integer**| Only return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

