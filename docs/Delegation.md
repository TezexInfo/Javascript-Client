# TezexApi.Delegation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**branch** | **String** |  | [optional] 
**source** | **String** |  | [optional] 
**publicKey** | **String** |  | [optional] 
**fee** | **Integer** |  | [optional] 
**counter** | **Integer** |  | [optional] 
**delegate** | **String** |  | [optional] 
**level** | **Integer** |  | [optional] 
**blockHash** | **String** |  | [optional] 
**time** | **Date** |  | [optional] 


