# TezexApi.TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_int** | **String** |  | [optional] 
**_string** | **String** |  | [optional] 
**prim** | **String** |  | [optional] 
**args** | [**[TezosScript]**](TezosScript.md) |  | [optional] 


