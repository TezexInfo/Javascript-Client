# TezexApi.MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


<a name="candlestick"></a>
# **candlestick**
> [Candlestick] candlestick(denominator, numerator, period)

Candlestick Data

Returns CandleStick Prices

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.MarketApi();

var denominator = "denominator_example"; // String | which currency

var numerator = "numerator_example"; // String | to which currency

var period = "period_example"; // String | Timeframe of one candle


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.candlestick(denominator, numerator, period, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **String**| which currency | 
 **numerator** | **String**| to which currency | 
 **period** | **String**| Timeframe of one candle | 

### Return type

[**[Candlestick]**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="ticker"></a>
# **ticker**
> Ticker ticker(numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.MarketApi();

var numerator = "numerator_example"; // String | The level of the Blocks to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ticker(numerator, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **String**| The level of the Blocks to retrieve | 

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

