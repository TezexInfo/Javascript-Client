# TezexApi.TransactionRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxBlockLevel** | **Integer** |  | [optional] 
**transactions** | [**[Transaction]**](Transaction.md) |  | [optional] 
**totalResults** | **Integer** |  | [optional] 
**currentPage** | **Integer** |  | [optional] 


