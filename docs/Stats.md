# TezexApi.Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statGroup** | **String** |  | [optional] 
**stat** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**start** | **Date** |  | [optional] 
**end** | **Date** |  | [optional] 


