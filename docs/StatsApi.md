# TezexApi.StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStatistics**](StatsApi.md#getStatistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**getStatsOverview**](StatsApi.md#getStatsOverview) | **GET** /stats/overview | Returns some basic Info


<a name="getStatistics"></a>
# **getStatistics**
> [Stats] getStatistics(group, stat, period, opts)

Get Statistics

Get Statistics

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.StatsApi();

var group = "group_example"; // String | Block, Transaction, etc

var stat = "stat_example"; // String | 

var period = "period_example"; // String | 

var opts = { 
  'startTime': new Date("2013-10-20T19:20:30+01:00"), // Date | 
  'endTime': new Date("2013-10-20T19:20:30+01:00") // Date | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStatistics(group, stat, period, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **String**| Block, Transaction, etc | 
 **stat** | **String**|  | 
 **period** | **String**|  | 
 **startTime** | **Date**|  | [optional] 
 **endTime** | **Date**|  | [optional] 

### Return type

[**[Stats]**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getStatsOverview"></a>
# **getStatsOverview**
> StatsOverview getStatsOverview()

Returns some basic Info

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.StatsApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getStatsOverview(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

