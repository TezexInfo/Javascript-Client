# TezexApi.TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTransaction**](TransactionApi.md#getTransaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**getTransactionsRecent**](TransactionApi.md#getTransactionsRecent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactionsAll**](TransactionApi.md#transactionsAll) | **GET** /transactions/all | Get All Transactions
[**transactionsByLevelRange**](TransactionApi.md#transactionsByLevelRange) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


<a name="getTransaction"></a>
# **getTransaction**
> Transaction getTransaction(transactionHash)

Get Transaction

Get a specific Transaction

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.TransactionApi();

var transactionHash = "transactionHash_example"; // String | The hash of the Transaction to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransaction(transactionHash, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transactionHash** | **String**| The hash of the Transaction to retrieve | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="getTransactionsRecent"></a>
# **getTransactionsRecent**
> [Transaction] getTransactionsRecent()

Returns the last 50 Transactions

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.TransactionApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransactionsRecent(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="transactionsAll"></a>
# **transactionsAll**
> TransactionRange transactionsAll(opts)

Get All Transactions

Get all Transactions

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.TransactionApi();

var opts = { 
  'page': 3.4, // Number | Pagination, 200 tx per page max
  'order': "order_example", // String | ASC or DESC
  'limit': 56 // Integer | Results per Page
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.transactionsAll(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

<a name="transactionsByLevelRange"></a>
# **transactionsByLevelRange**
> TransactionRange transactionsByLevelRange(startlevel, stoplevel, opts)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example
```javascript
var TezexApi = require('TezexAPI');

var apiInstance = new TezexApi.TransactionApi();

var startlevel = 3.4; // Number | lowest blocklevel to return

var stoplevel = 3.4; // Number | highest blocklevel to return

var opts = { 
  'page': 3.4, // Number | Pagination, 200 tx per page max
  'order': "order_example", // String | ASC or DESC
  'limit': 56 // Integer | Results per Page
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.transactionsByLevelRange(startlevel, stoplevel, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Number**| lowest blocklevel to return | 
 **stoplevel** | **Number**| highest blocklevel to return | 
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **String**| ASC or DESC | [optional] 
 **limit** | **Integer**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

