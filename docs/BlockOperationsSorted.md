# TezexApi.BlockOperationsSorted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**[Transaction]**](Transaction.md) |  | [optional] 
**originations** | [**[Origination]**](Origination.md) |  | [optional] 
**delegations** | [**[Delegation]**](Delegation.md) |  | [optional] 
**endorsements** | [**[Endorsement]**](Endorsement.md) |  | [optional] 


